.PHONY:
build-ubuntu:
ifndef PATCH
	curl http://cdimage.ubuntu.com/ubuntu-base/releases/$(MAJOR).$(MINOR)/release/ubuntu-base-$(MAJOR).$(MINOR)-base-amd64.tar.gz >ubuntu-root-$(MAJOR).$(MINOR).tar.gz
else
	curl http://cdimage.ubuntu.com/ubuntu-base/releases/$(MAJOR).$(MINOR)/release/ubuntu-base-$(PATCH)-base-amd64.tar.gz >ubuntu-root-$(MAJOR).$(MINOR).tar.gz
endif
	sha256sum ubuntu-root-$(MAJOR).$(MINOR).tar.gz
	docker build --no-cache -t $(FITS_REGISTRY_IMAGE):$(MAJOR).$(MINOR) -t $(REGISTRY_IMAGE):$(MAJOR).$(MINOR) --build-arg MAJOR=$(MAJOR) --build-arg MINOR=$(MINOR) -f Dockerfile.ubuntu .

.PHONY:
build-and-push-ubuntu: build-ubuntu
	docker push  $(REGISTRY_IMAGE):$(MAJOR).$(MINOR)
	docker login -u $(FITS_REGISTRY_USER) -p $(FITS_REGISTRY_PASSWORD) $(FITS_BASE)
	docker push  $(FITS_REGISTRY_IMAGE):$(MAJOR).$(MINOR)

.PHONY:
build-and-push-ubuntu-latest:
	docker tag $(REGISTRY_IMAGE):$(MAJOR).$(MINOR) $(REGISTRY_IMAGE):latest
	docker tag $(REGISTRY_IMAGE):$(MAJOR).$(MINOR) $(FITS_REGISTRY_IMAGE):latest
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker push  $(REGISTRY_IMAGE):latest
	docker login -u $(FITS_REGISTRY_USER) -p $(FITS_REGISTRY_PASSWORD) $(FITS_BASE)
	docker push  $(FITS_REGISTRY_IMAGE):latest

.PHONY:
build-debian:
	curl -L https://github.com/debuerreotype/docker-debian-artifacts/raw/dist-amd64/$(VERSION)/slim/rootfs.tar.xz >debian-slim-root-$(VERSION).tar.xz
	sha256sum debian-slim-root-$(VERSION).tar.xz
	docker build --no-cache -t $(FITS_REGISTRY_IMAGE):$(VERSION)-slim -t $(REGISTRY_IMAGE):$(VERSION)-slim --build-arg VERSION=$(VERSION) -f Dockerfile.debian .

.PHONY:
build-and-push-debian: build-debian
	docker push  $(REGISTRY_IMAGE):$(VERSION)-slim
	docker login -u $(FITS_REGISTRY_USER) -p $(FITS_REGISTRY_PASSWORD) $(FITS_BASE)
	docker push  $(FITS_REGISTRY_IMAGE):$(VERSION)-slim
